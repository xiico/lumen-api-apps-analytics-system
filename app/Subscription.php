<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $table = 'subscriptions';
    
    protected $fillable = [
        'user_id', 
        'device_id',
        'used_free_trial',
        'free_trial_id',
        'is_subscribed',
        'subscription_id',
        'updated_at', 
    ];

    public function devices()
    {
        return $this->belongsTo('App\DevicesInfo', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'id');
    }

    public function getSubscriptions()
    {
        $query = $this;
        return $query->paginate(20);
    }

    public function getSubscription($id)
    {
        return $this->find($id);
    }

    public function createSubscription($input)
    {
        return $this->create($input->all());
    }

    public function updateSubsciption($id, $input)
    {
        $updated = $this->find($id)->update($input);
        $subscription = $this->find($id);

        if($updated) {
            return $subscription;
        }

        return false;
    }
}