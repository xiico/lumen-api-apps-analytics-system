<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use App\User;

class Authenticate {

    protected $auth;

    public function __construct(Auth $auth) {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next, $guard = null) {
        if ($this->auth->guard($guard)->guest()) {
            if ($request->has('api_token')) {
                try {
                    $token = $request->input('api_token');
                    $check_token = User::where('api_token', $token)->first();
                    if ($check_token) {
                        $res['success'] = false;
                        $res['errorCode'] = 401;
                        // $res['errorMessage'] = 'Unauthenticated';
                        $res['errorMessage'] = 'api_token_required_error';
                        return response($res);
                    }else{
                        $res['success'] = false;
                        $res['errorCode'] = 402;
                        // $res['errorMessage'] = 'Unauthorized';
                        $res['errorMessage'] = 'api_token_wrong_error';
                        return response($res);
                    }
                } catch (\Illuminate\Database\QueryException $ex) {
                        $res['success'] = false;
                        $res['errorCode'] = 102;
                        $res['errorMessage'] = "db_error";
                        if(env('APP_DEBUG')){
                            $res['errorDetail'] = $exception->getMessage();
                        }
                    return response($res);
                }
            } else {
                $res['success'] = false;
                $res['errorCode'] = 401;
                // $res['errorMessage'] = 'Login please!';
                $res['errorMessage'] = 'api_token_required_error';
                return response($res);
            }
        }
        return $next($request);
    }

}
