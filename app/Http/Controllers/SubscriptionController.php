<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Auth;
use App\Device;
use App\User;
use App\Subscription;
use Validator;


class SubscriptionController extends Controller {

    public function addSubscription(Request $request) {

        $rules = [
            'device_id' => 'required',
            'used_free_trial' => 'required',
            'free_trial_id' => 'required',
            'is_subscribed' => 'required',
            'subscription_id' => 'required',
        ];

        $customMessages = [
            'device_id.required' => '241:attribute_required_error',
            'used_free_trial.required' => '242:attribute_required_error',
            'free_trial_id.required' => '243:attribute_required_error',
            'is_subscribed.required' => '244:attribute_required_error',
            'subscription_id.required' => '245:attribute_required_error',
        ];
        // $this->validate($request, $rules, $customMessages);
        $validator = Validator::make($request->all(), $rules,$customMessages);
        if ($validator->fails()) { 
            $res['success'] = false;
            $res['errorCode'] = substr($validator->errors()->first(),0,3);
            $res['errorMessage'] = substr($validator->errors()->first(),3);
            return response($res,201);
        }

        try {
            $user_id = Auth::id();
            $device_id = $request->input('device_id');
            $used_free_trial = $request->input('used_free_trial');
            $free_trial_id = $request->input('free_trial_id');
            $is_subscribed = $request->input('is_subscribed');
            $subscription_id = $request->input('subscription_id');

            $save = Subscription::create([
                        'user_id' => $user_id,
                        'device_id' => $device_id,
                        'used_free_trial' => $used_free_trial,
                        'free_trial_id' => $free_trial_id,
                        'is_subscribed' => $is_subscribed,
                        'subscription_id' => $subscription_id,
            ]);
            $res['success'] = true;
            $res['message'] = 'Subscription info added successfully!';
            $res['data'] = $save;
            return response($res, 200);
        } catch (\Illuminate\Database\QueryException $ex) {
            $res['success'] = false;
            $$res['errorCode'] = 102;
            $res['errorMessage'] = "db_error";
            if(env('APP_DEBUG')){
                $res['errorDetail'] = $ex->getMessage();
            }
            return response($res, 500);
        }
    }


    
    public function updateSubscription(Request $request) {

        $rules = [
            'device_id' => 'required',
            'used_free_trial' => 'required',
            'free_trial_id' => 'required',
            'is_subscribed' => 'required',
            'subscription_id' => 'required',
        ];

        $customMessages = [
            'device_id.required' => '241:attribute_required_error',
            'used_free_trial.required' => '242:attribute_required_error',
            'free_trial_id.required' => '243:attribute_required_error',
            'is_subscribed.required' => '244:attribute_required_error',
            'subscription_id.required' => '245:attribute_required_error',
        ];
        // $this->validate($request, $rules, $customMessages);
        $validator = Validator::make($request->all(), $rules,$customMessages);
        if ($validator->fails()) { 
            $res['success'] = false;
            $res['errorCode'] = substr($validator->errors()->first(),0,3);
            $res['errorMessage'] = substr($validator->errors()->first(),3);
            return response($res);
        }

        try {
            $user_id = Auth::id();
            $device_id = $request->input('device_id');
            $id = $request->input('id');//subscription id in database
            $used_free_trial = $request->input('used_free_trial');
            $free_trial_id = $request->input('free_trial_id');
            $is_subscribed = $request->input('is_subscribed');
            $subscription_id = $request->input('subscription_id');

            $save = Subscription::whereId($id)->update([
                        'user_id' => $user_id,
                        'device_id' => $device_id,
                        'used_free_trial' => $used_free_trial,
                        'free_trial_id' => $free_trial_id,
                        'is_subscribed' => $is_subscribed,
                        'subscription_id' => $subscription_id
            ]);

            $data = Subscription::whereId($id)->first();
            $res['success'] = true;
            $res['message'] = 'Subscription updated successfully!';
            $res['data'] = $data;
            return response($res);
        } catch (\Illuminate\Database\QueryException $ex) {
            $res['success'] = false;
            $res['errorCode'] = 102;
            $res['errorMessage'] = "db_error";
            if(env('APP_DEBUG')){
                $res['errorDetail'] = $ex->getMessage();
            }
            return response($res);
        }
    }


    //get all subscriptions details
    public function getAllSubscriptions(Request $request) {
        $user_id = Auth::id();
        $data = Subscription::whereUser_id($user_id)->get();
        if ($data) {
            $res['success'] = true;
            $res['message'] = 'All Subscriptions.';
            $res['data'] = $data;
            return response($res);
        } else {
            $res['success'] = false;
            $res['errorCode'] = 400;
            $res['errorMessage'] = 'user_data_not_existing';
            return response($res);
        }
    }

}
