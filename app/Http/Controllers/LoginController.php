<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Hash;
use App\User;
use Validator;

class LoginController extends Controller {

    public function login(Request $request) {

        $rules = [
            'email' => 'bail|required|email',
            'password' => 'required'
        ];

        $customMessages = [
            'email.required' => '202:attribute_required_error',
            'email.email' => '203:attribute_format_error',
            'password.required' => '205:attribute_required_error',
        ];
        //$this->validate($request, $rules, $customMessages);
        $validator = Validator::make($request->all(), $rules,$customMessages);
        if ($validator->fails()) { 
            $res['success'] = false;
            $res['errorCode'] = substr($validator->errors()->first(),0,3);
            $res['errorMessage'] = substr($validator->errors()->first(),3);
            return response($res);
        }
        
        $email = $request->input('email');
        try {
            $login = User::where('email', $email)->first();
            if ($login) {
                if ($login->count() > 0) {
                    if (Hash::check($request->input('password'), $login->password)) {
                        try {

                            if ($login->remember_token) {
                                $api_token = $login->api_token;
                            } else {
                                $api_token = sha1($login->id_user . time());

                                $create_token = User::where('id', $login->id)->update(['api_token' => $api_token,'remember_token'=>true]);
                            }
                            $res['success'] = true;
                            $res['data'] = $login;
                            $res['api_token'] = $api_token;

                            return response($res);
                        } catch (\Illuminate\Database\QueryException $ex) {
                            $res['success'] = false;
                            $res['errorCode'] = 101;
                            $res['errorMessage'] = "unknown_error";
                            if(env('APP_DEBUG')){
                                $res['errorDetail'] = $ex->getMessage();
                            }
                            return response($res);
                        }
                    } else {
                        $res['success'] = false;
                        $res['errorCode'] = 300;
                        $res['errorMessage'] = 'username_or_password_incorrect';
                        return response($res);
                    }
                } else {
                    $res['success'] = false;
                    $res['errorCode'] = 300;
                    $res['errorMessage'] = 'username_or_password_incorrect';
                    return response($res);
                }
            } else {
                $res['success'] = false;
                $res['errorCode'] = 300;
                $res['errorMessage'] = 'username_or_password_incorrect';
                return response($res);
            }
        } catch (\Illuminate\Database\QueryException $ex) {
            $res['success'] = false;
            $res['errorCode'] = 101;
            $res['errorMessage'] = "unknown_error";
            if(env('APP_DEBUG')){
                $res['errorDetail'] = $ex->getMessage();
            }
            return response($res);
        }
    }

}
