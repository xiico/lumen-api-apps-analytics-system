errorcodes.txt
Here are some error code and error message. We can think more and add more in them.

//system errors
	null_error = 100,
	unknown_error = 101,
	db_error = 102,

//user Data errors (200-220)
	username_required_error = 200,
	username_already_taken_error = 201,
	email_required_error = 202,
	email_format_error = 203,
	email_already_taken_error = 204,
	password_required_error = 205,
	usage_required_error = 206,
	gender_required_error = 207,
	age_required_error = 208,
	age_shouldbe_numeric_error = 209,

//Device Data Errors (221-240)
	platform_required_error = 221,
	udid_required_error = 222,
	udid_already_taken_error = 223,
	installation_date_required_error = 224,
	number_of_launches_required_error = 225,
	place_of_use_required_error = 226,
	updated_at_required_error = 227,

//Subscription Data Errors (241-260)
	device_id_required_error = 241,
	used_free_trial_required_error = 242,
	free_trial_id_required_error = 243,
	is_subscribed_required_error = 244,
	subscription_id_required_error = 245,


//security error
	username_or_password_incorrect = 300,
	device_token_and_user_guid_not_valid = 301,
	unauthorized_api_use = 303,
	user_guid_and_device_id_invalid = 304,

//Common Authentication & Data errors
	user_data_not_existing = 400,
	api_token_required_error = 401,
	api_token_wrong_error = 402,
	user_data_already_registered = 403,
	user_data_invalid_character = 404,

//subscription errors
	credit_plan_not_available = 500