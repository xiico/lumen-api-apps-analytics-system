<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Auth;
use App\User;
use Validator;


class UserController extends Controller {

//Register new user 
    public function register(Request $request) {
        $rules = [
            'username' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'usage' => 'required',
            'gender' => 'required',
            'age' => 'required|numeric'
        ];

        $customMessages = [
            'username.required' => '200:attribute_required_error',
            'username.unique' => '201:attribute_already_taken_error',
            'email.required' => '202:attribute_required_error',
            'email.email' => '203:attribute_format_error',
            'email.unique' => '204:attribute_already_taken_error',
            'password.required' => '205:attribute_required_error',
            'usage.required' => '206:attribute_required_error',
            'gender.required' => '207:attribute_required_error',
            'age.required' => '208:attribute_required_error',
            'age.numeric' => '209:attribute_shouldbe_numeric_error',
        ];
        // $this->validate($request, $rules, $customMessages);
        $validator = Validator::make($request->all(), $rules,$customMessages);
        if ($validator->fails()) { 
            $res['success'] = false;
            $res['errorCode'] = substr($validator->errors()->first(),0,3);
            $res['errorMessage'] = substr($validator->errors()->first(),3);
            return response($res);
        }

        try {
            $hasher = app()->make('hash');
            $username = $request->input('username');
            $email = $request->input('email');
            $password = $hasher->make($request->input('password'));

            $usage = $request->input('usage');
            $gender = $request->input('gender');
            $age = $request->input('age');
            $installation_date = $request->input('installation_date');
            $number_of_launches = $request->input('number_of_launches');
            $place_of_use = $request->input('place_of_use');
            $api_token = sha1($username . time());

            $data = User::create([
                        'username' => $username,
                        'email' => $email,
                        'password' => $password,
                        'api_token' => '',
                        'usage' => $usage,
                        'gender' => $gender,
                        'age' => $age, 
                        'api_token' => $api_token,
                        'remember_token'=>true
            ]);
            $res['success'] = true;
            $res['data'] = $data;
            $res['api_token'] = $api_token;
            return response($res);
        } catch (\Illuminate\Database\QueryException $ex) {
            $res['success'] = false;
            $res['errorCode'] = 102;
            $res['errorMessage'] = "db_error";
            if(env('APP_DEBUG')){
                $res['errorDetail'] = $ex->getMessage();
            }
            //$res['errorMessage'] = $ex->getMessage();
            return response($res);
        }
    }

//get others users details except the current logged in
    public function get_otherUsers(Request $request) {
        $token = $request->input('api_token');
        $cuser = User::where('api_token',$token)->first();
        if($cuser!=null)
        {
            $user = User::all()->except($cuser['id']);
            if (!$user->isEmpty()) {
                $res['success'] = true;
                $res['data'] = $user;
                return response($res);
            } else {
                $res['success'] = false;
                $res['errorCode'] = 400;
                $res['errorMessage'] = 'user_data_not_existing';

                return response($res);
            }
        }else{
            $res['success'] = false;
            $res['errorCode'] = 301;
            $res['errorMessage'] = 'device_token_and_user_guid_not_valid';
            return response($res);
        }
    }

//get all users details
    public function get_AllUser() {
        $user = User::all();
        if ($user) {
            $res['success'] = true;
            $res['data'] = $user;
            return response($res);
        } else {
            $res['success'] = false;
            $res['errorCode'] = 400;
            $res['errorMessage'] = 'user_data_not_existing';
            return response($res,201);
        }
    }
    
//TODO: get individual user detail
    public function get_profile(Request $request) {
        $token = $request->input('api_token');
        $user = User::where('api_token',$token)->first();
        if ($user) {
            $res['success'] = true;
            $res['data'] = $user;
            return response($res);
        } else {
            $res['success'] = false;
            $res['errorCode'] = 400;
            $res['errorMessage'] = 'user_data_not_existing';

            return response($res,201);
        }
    }

//Test Function
    public function testResponse(Request $request)
    {
        $token = $request->input('api_token');
        $user  = User::where('api_token', $token)->first();

        if ($user) {

            $rules = [
                'email' => 'required',
                'password' => 'required'
            ];

            $customMessages = [
                'required' => ':attribute can not be empty'
            ];

            $validator = Validator::make($request->all(), $rules,$customMessages);
            if ($validator->fails()) { 
                $res['success'] = false;
                $res['errorCode'] = 201;
                $res['errorMessage'] = "Please enter a valid email / password.";
                return response($res,201);
            }

            $data['success'] = true;
            $data['message'] = 'Success';
            $data['data']    = array('email' => $request->email, 'password' => $request->password);
            return response($data);
        } else {
            $res['success'] = false;
            $res['errorCode'] = 201;
            $res['errorMessage'] = 'Cannot find user!';
            return response($res,201);
        }
    }

}
