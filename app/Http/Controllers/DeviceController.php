<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Auth;
use App\Device;
use App\User;
use Validator;


class DeviceController extends Controller {

    public function addDevice(Request $request) {

        $rules = [
            'platform' => 'required',
            'udid' => 'required|unique:devices',
            'installation_date' => 'required',
            'number_of_launches' => 'required',
            'place_of_use' => 'required',
        ];

        $customMessages = [
            'platform.required' => '221:attribute_required_error',
            'udid.required' => '222:attribute_required_error',
            'udid.unique' => '223:attribute_already_taken_error',
            'installation_date.required' => '224:attribute_required_error',
            'number_of_launches.required' => '225:attribute_required_error',
            'place_of_use.required' => '226:attribute_required_error',
        ];
        // $this->validate($request, $rules, $customMessages);
        $validator = Validator::make($request->all(), $rules,$customMessages);
        if ($validator->fails()) { 
            $res['success'] = false;
            $res['errorCode'] = substr($validator->errors()->first(),0,3);
            $res['errorMessage'] = substr($validator->errors()->first(),3);
            return response($res);
        }

        try {
            $user_id = Auth::id();
            $platform = $request->input('platform');
            $udid = $request->input('udid');
            $installation_date = $request->input('installation_date');
            $number_of_launches = $request->input('number_of_launches');
            $place_of_use = $request->input('place_of_use');

            $saveData = Device::create([
                        'user_id' => $user_id,
                        'platform' => $platform,
                        'udid' => $udid,
                        'installation_date' => $installation_date,
                        'number_of_launches' => $number_of_launches,
                        'place_of_use' => $place_of_use,
            ]);
            $res['success'] = true;
            $res['message'] = 'Device added successfully!';
            $res['data'] = $saveData;
            return response($res);
        } catch (\Illuminate\Database\QueryException $ex) {
            $res['success'] = false;
            $res['errorCode'] = 102;
            $res['errorMessage'] = "db_error";
            if(env('APP_DEBUG')){
                $res['errorDetail'] = $ex->getMessage();
            }
            return response($res);
        }
    }


    
    public function updateDeviceLaunches(Request $request) {

        $rules = [
            'udid' => 'required',
            'number_of_launches' => 'required',
            'updated_at' => 'required',
        ];

        $customMessages = [
            'udid.required' => '222:attribute_required_error',
            'number_of_launches.required' => '225:attribute_required_error',
            'updated_at.required' => '227:attribute_required_error',
        ];
        //$this->validate($request, $rules, $customMessages);
        $validator = Validator::make($request->all(), $rules,$customMessages);
        if ($validator->fails()) { 
            $res['success'] = false;
            $res['errorCode'] = substr($validator->errors()->first(),0,3);
            $res['errorMessage'] = substr($validator->errors()->first(),3);
            return response($res);
        }
        try {
            $user_id = Auth::id();
            $udid = $request->input('udid');
            $number_of_launches = $request->input('number_of_launches');
            $updated_at = $request->input('updated_at');

            $update = Device::whereUdid($udid)->update([
                        'user_id' => $user_id,
                        'udid' => $udid,
                        'number_of_launches' => $number_of_launches,
                        'updated_at' => $updated_at,
            ]);

            $data = Device::whereUdid($udid)->first();
            $res['success'] = true;
            $res['message'] = 'Number of launches updated successfully!';
            $res['data'] = $data;
            return response($res);
        } catch (\Illuminate\Database\QueryException $ex) {
            $res['success'] = false;
            $res['errorCode'] = 102;
            $res['errorMessage'] = "db_error";
            if(env('APP_DEBUG')){
                $res['errorDetail'] = $ex->getMessage();
            }
            return response($res);
        }
    }


    //get all subscriptions details
    public function getAllDevices(Request $request) {
        $user_id = Auth::id();
        $data = Device::whereUser_id($user_id)->get();
        if (!$data->isEmpty()) {
            $res['success'] = true;
            $res['message'] = 'All Devices.';
            $res['data'] = $data;
            return response($res);
        } else {
            $res['success'] = false;
            $res['errorCode'] = 400;
            $res['errorMessage'] = 'user_data_not_existing';
            return response($res);
        }
    }

}
