<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $table = 'devices';
    
    protected $fillable = [
        'user_id', 
        'platform', 
        'udid', 
        'installation_date', 
        'number_of_launches', 
        'place_of_use', 
        'updated_at', 
    ];

    public function subscriptions()
    {
        $this->hasMany('App\Subscription', 'device_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'id');
    }

    public function getDevices()
    {
        $query = $this;
        return $query->paginate(20);
    }

    public function getDevice($id)
    {
        return $this->find($id);
    }

    public function createDevice($input)
    {
        return $this->create($input->all());
    }

    public function updateDevice($id, $input)
    {
        $updated = $this->find($id)->update($input);
        $post = $this->find($id);

        if($updated) {
            return $post;
        }

        return false;
    }
}