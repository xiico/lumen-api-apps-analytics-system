<?php
//php -S localhost:8000 -t public public/index.php

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//Users API
$router->group(['prefix' => 'api/v1/user'], function () use ($router) {

    $router->post('/login', 'LoginController@login');
    $router->post('/register', 'UserController@register');
    $router->get('/profile', ['middleware' => 'auth', 'uses' => 'UserController@get_profile']);
    $router->get('/all', ['middleware' => 'auth', 'uses' => 'UserController@get_otherUsers']);
    $router->post('/test', ['middleware' => 'auth', 'uses' => 'UserController@testResponse']);
});

//Devices API
$router->group(['prefix' => 'api/v1/device'], function () use ($router) {
    $router->post('/', ['middleware' => 'auth', 'uses' => 'DeviceController@addDevice']);

    $router->put('/launches', ['middleware' => 'auth', 'uses' => 'DeviceController@updateDeviceLaunches']);

    $router->get('/all', ['middleware' => 'auth', 'uses' => 'DeviceController@getAllDevices']);
    $router->get('/{id}', ['middleware' => 'auth', 'uses' => 'DeviceController@getDevice']);

    $router->delete('/{id}', ['middleware' => 'auth', 'uses' => 'DeviceController@deleteDevice']);

});

//Subscriptions API
$router->group(['prefix' => 'api/v1/subscription'], function () use ($router) {
    $router->post('/', ['middleware' => 'auth', 'uses' => 'SubscriptionController@addSubscription']);

    $router->put('/', ['middleware' => 'auth', 'uses' => 'SubscriptionController@updateSubscription']);

    $router->get('/all', ['middleware' => 'auth', 'uses' => 'SubscriptionController@getAllSubscriptions']);
    $router->get('/{id}', ['middleware' => 'auth', 'uses' => 'SubscriptionController@getSubscription']);

    $router->delete('/{id}', ['middleware' => 'auth', 'uses' => 'SubscriptionController@deleteSubscription']);

});
